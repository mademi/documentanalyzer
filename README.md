# Document Analyzer

The purpose of this project is to provide a simple REST interface for querying the word frequency for text files. It should provide a interface for posting a file, and then asynchronously process the file and provide a REST interface for retrieving a word frequency summary. 


## Event Architecture
![alt text](assets/diagram.png "Title")



## API Design

| #  | Endpoint                             | Method | Description                                          | Parameters                                             | Responses                  |
|----|--------------------------------------|--------|------------------------------------------------------|--------------------------------------------------------|----------------------------|
| 1  | `POST /v1/documents/word-frequency`               | POST   | Initiates a word frequency analysis job for a document. | `command` (WordFrequencyUploadCommand): The text file to be analyzed. | **201 (Created):** Location and document ID in response body.<br>**400 (Bad Request):** Error details if operation fails. |
| 2  | `GET /v1/documents/word-frequency/{id:guid}`      | GET    | Retrieves the word frequency summary report for a specific document. | `id` (GUID): Unique identifier of the document.      | **200 (OK):** File stream of the summary report.<br>**404 (Not Found):** If report cannot be located (processing). |
| 3  | `GET /v1/documents/word-frequency/{id:guid}/original` | GET    | Provides a download for the original document as submitted. | `id` (GUID): Unique identifier of the document.      | **200 (OK):** File stream of the original document.<br>**404 (Not Found):** If document cannot be located. |

### Events

| Event Name               | Description                                                       | Properties                                          |
|--------------------------|-------------------------------------------------------------------|-----------------------------------------------------|
| `DocumentCreatedEvent`   | Triggered when a document is uploaded and stored in bucket storage. | `Id`: Unique identifier for the document.<br>`BucketName`: Name of the bucket where the file is stored.<br>`OriginalFileKey`: Key for the original file in the storage. |
| `DocumentProcessedEvent` | Published after the document has been processed to report the outcome. | `Id`: Unique identifier for the document linked to the event.<br>`WordFrequencyFileKey`: Storage key for the word frequency file.<br>`IsProcessed`: Boolean indicating if the processing was successful. |

These events are part of an asynchronous, event-driven system that uses RabbitMQ and MassTransit for message passing, aiding in the orchestration of document processing tasks.


## Running the solution

To start:

```console
make start
```

To stop: 

```console
make stop
```

## Notes, documents, nice to know

There is a lot of things that need to be addressed, for this to be considered production grade. Some points to note:
- Connection resiliency for dependencies, and retry patterns for failed connections, initial loads etc..
- Retry, circuit breakers and resiliency, fallbacks, health checks (built into .NET core, should be enabled, separate readiness and health probe endpoints ideally)
- Metrics is supported using Prometheus, but custom metrics should be implemented for instrumentation in areas that should be instrumented.
- Transactional Outbox pattern and other patterns which improve the reliability of our message transmission, and data consistency.
- Lots of 'happy pathing', exceptions not caught and handled correctly - this should be rewritten, I used the result pattern across the solution to provide a clean, and easy way to manage errors or the result. 
- CI / CD pipelines, with multi stage (e.g., build, test, release, deploy)
- Secrets to override the environment variables, preferably injected during the CI / CD pipeline execution flows using OIDC from GitLab/GitHub to some external key/secret store (short lived access tokens to retrieve secrets)
- Consumer was integrated as a stand-alone service, primarily because this detaches the processing of heavy tasks to a separate service, improving availability/scalability but also introducing much needed load leveling in the system. Something also important, is that it allows us to independently scale the workers / consumers, by adding more replicas (e.g., maintaining one instance of the API, but having 3-4 instances of the consumers sharing the load from the queues). 
- Logging, structured logging using JSON (easily configurable)
- Test pyramid (e.g., unit test, integration tests, e2e tests) ideally introduced, and available during intake of MRs. 
- Improved abstraction layers, and domain models (anemic models at the moment), whilst this is fine for a prototype, as complexity grows and the codebase gets bigger it is better to transition to rich domain models.
- Ideally, the 'polling' should be transitioned into a more real-time protocol e.g., web sockets / signalr, mqtt, xmpp or equivalents. This would allow us to subscribe to the processed file, and get notified instantly, e.g., providing the best of both worlds. 


## Deployment of solution, design decisions

The solution is already built for the cloud and could easily be deployed to Azure, AWS or GCP or other cloud platforms. I primarily design solutions, using a cloud agnostic architecture and try to avoid relying too much on particular vendor lock in offerings, and usually create or use some abstraction layer library (e.g., MassTransit to easily be able to transition between various brokers such as RabbitMQ, SQS etc..).

- Use terraform to deploy a K8S cluster, and other (managed offerings) that we would like to use. E.g., SQS, RDS. 
    - Create separate infrastructure repository for the terraform configurations
    - Use CI / CD to automate the provisioning of resources
    - We should have separate environments (e.g., dev, staging, prod)
    - Load balancers (nginx, traefik or equivalent) should be used to help distribute load between services 
    - Very much recommended to use version control for the Terraform configurations so changes are auditable, trackable and reversible.
- Create helm charts for the application, use Helm charts for other infrastructure components we would like to deploy
    - The helm charts could either be stored in each services repository, or centralized in a repository
    - The helm charts should be deployed upon a new release, depending on what release strategy we opt for.  
    - Define health, readiness probes for Helm
    - Very much recommended to use version control for the helm charts so changes are auditable, trackable and reversible.
    - Inject environment variables from K8S secrets, especially for sensitive data (connection strings, private keys, access keys, tokens)
- CI / CD could be achieved using GitLab CI, GitHub Actions, CircleCI Jenkins or others.
    - Private registries for pushing container images, NuGet packages etc.
    - It is important that the CI / CD pipelines are executed in the right flows, prevention of MRs, incase of test failures, and that good test coverage is present at all cycles.
    - Static code analysis, container image scanning prior to deployments
 

Deployment to the K8S cluster through the CI / CD could be achieved with the use of `kubectl`, in other words, deployment and configuration of infrastructure components should be managed by Terraform, while `kubectl` can be used to integrate the CI / CD flows to deploy to the K8S cluster. Furthermore, with production grade applications and infrastructures you should have a centralized logging architecture, and this can be built using the EFK stack or equivalent. Fluentd can be configured to send logs to ElasticSearch, which can then be explored using Kibana for a open source logging infrastructure. 

