﻿using DocumentAnalyzer.Domain.Storage;
using DocumentAnalyzer.Infrastructure.Persistence;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Moq;

namespace DocumentAnalyzer.Api.IntegrationTests.Base
{
    public class CustomWebApplicationFactory<TProgram>
        : WebApplicationFactory<TProgram> where TProgram : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.RemoveAll(typeof(DbContextOptions<ApplicationDbContext>));

                var mockStorageClient = new Mock<IStorageClient>();
                mockStorageClient.Setup(x => x.UploadFileAsync(It.IsAny<IFormFile>(), It.IsAny<string>()))
                                 .Returns(Task.CompletedTask);

                var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(IStorageClient));
                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }

                services.AddSingleton<IStorageClient>(options => mockStorageClient.Object);

                services.AddDbContext<ApplicationDbContext>(options =>
                {
                    options.UseInMemoryDatabase("documents");
                });

                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var context = scopedServices.GetRequiredService<ApplicationDbContext>();
                    var logger = scopedServices.GetRequiredService<ILogger<CustomWebApplicationFactory<TProgram>>>();

                    context.Database.EnsureCreated();
                }
            });

            builder.UseEnvironment("Development");
        }
    }

}
