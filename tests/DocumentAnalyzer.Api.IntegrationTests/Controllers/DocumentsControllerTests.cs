﻿using DocumentAnalyzer.Api.IntegrationTests.Base;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Text;

namespace DocumentAnalyzer.Api.IntegrationTests.Controllers
{
    public class DocumentsControllerTests : IClassFixture<CustomWebApplicationFactory<Program>>
    {
        private readonly CustomWebApplicationFactory<Program> _factory;

        public DocumentsControllerTests(CustomWebApplicationFactory<Program> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task Post_InalidWordFrequencyFileUploadCommand_ReturnsBadRequestErrorResult()
        {
            // Arrange
            var client = _factory.CreateClient();
            var file = CreateFormFile("", "image.png", "image/png");

            var formData = new MultipartFormDataContent();
            var fileContent = new StreamContent(file.OpenReadStream());
            fileContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/plain");
            formData.Add(fileContent, "file", file.FileName);

            // Act
            var response = await client.PostAsync("/v1/documents/word-frequency", formData);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }


        private static IFormFile CreateFormFile(string content, string fileName, string contentType)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(content);
            MemoryStream memoryStream = new MemoryStream(byteArray);
            IFormFile formFile = new FormFile(memoryStream, 0, memoryStream.Length, fileName, fileName)
            {
                Headers = new HeaderDictionary(),
                ContentType = contentType
            };

            return formFile;
        }

    }
}
