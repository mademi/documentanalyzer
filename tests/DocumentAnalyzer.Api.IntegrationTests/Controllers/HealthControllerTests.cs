﻿using DocumentAnalyzer.Api.IntegrationTests.Base;

namespace DocumentAnalyzer.Api.IntegrationTests.Controllers
{
    public class HealthControllerTests : IClassFixture<CustomWebApplicationFactory<Program>>
    {
        private readonly CustomWebApplicationFactory<Program> _factory;

        public HealthControllerTests(CustomWebApplicationFactory<Program> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GetHealth_ShouldReturnOk_WhenServiceIsAlive()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("v1/health");

            response.EnsureSuccessStatusCode();
        }

    }
}
