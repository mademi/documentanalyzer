﻿using DocumentAnalyzer.Application.Services;
using DocumentAnalyzer.Application.WordFrequencies.GetWordFrequencyDocumentById.Queries;
using DocumentAnalyzer.Domain.Entities;
using DocumentAnalyzer.Domain.Storage;
using ErrorOr;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;

namespace DocumentAnalyzer.Application.UnitTests.WordFrequencies.Queries
{
    public class WordFrequencyDocumentByIdQueryTests
    {
        private readonly Mock<IDocumentsService> _documentsService;
        private readonly Mock<IStorageClient> _storageClient;
        private readonly Mock<ILogger<GetWordFrequencyDocumentByIdQueryHandler>> _logger;

        public WordFrequencyDocumentByIdQueryTests()
        {
            _documentsService = new();
            _storageClient = new();
            _logger = new();
        }

        [Fact]
        public async void Handle_Should_ProvideStream_WhenQueryHandlerIsCalled()
        {
            // Arrange
            var id = Guid.NewGuid();
            var query = new GetWordFrequencyDocumentByIdQuery(id);
            var mockDocument = new Mock<Document>();
            mockDocument.Setup(x => x.Id).Returns(id);

            ErrorOr<Document> documentResult = mockDocument.Object;
            documentResult.Value.IsProcessed = true;

            _documentsService.Setup(x => x.GetDocumentById(It.IsAny<Guid>())).ReturnsAsync(documentResult);
            _storageClient.Setup(x => x.DownloadFileAsync(It.IsAny<string>())).ReturnsAsync(new MemoryStream());

            var handler = new GetWordFrequencyDocumentByIdQueryHandler(_storageClient.Object, _documentsService.Object, _logger.Object);

            // Act
            var result = await handler.Handle(query, CancellationToken.None);

            // Assert
            result.IsError.Should().BeFalse();
            result.Value.Should().NotBeNull();
        }

        [Fact]
        public async void Handle_Should_ReturnErrorWhenProcessingIncomplete_WhenQueryHandlerIsCalled()
        {
            // Arrange
            var id = Guid.NewGuid();
            var query = new GetWordFrequencyDocumentByIdQuery(id);
            var mockDocument = new Mock<Document>();
            mockDocument.Setup(x => x.Id).Returns(id);

            ErrorOr<Document> documentResult = mockDocument.Object;

            _documentsService.Setup(x => x.GetDocumentById(It.IsAny<Guid>())).ReturnsAsync(documentResult);
            _storageClient.Setup(x => x.DownloadFileAsync(It.IsAny<string>())).ReturnsAsync(new MemoryStream());

            var handler = new GetWordFrequencyDocumentByIdQueryHandler(_storageClient.Object, _documentsService.Object, _logger.Object);

            // Act
            var result = await handler.Handle(query, CancellationToken.None);

            // Assert
            result.IsError.Should().BeTrue();
        }

        [Fact]
        public async void Handle_Should_ReturnErrorWhenIdNotFound_WhenQueryHandlerIsCalled()
        {
            // Arrange
            var id = Guid.NewGuid();
            var query = new GetWordFrequencyDocumentByIdQuery(id);
            ErrorOr<Document> error = Error.NotFound();
            _documentsService.Setup(x => x.GetDocumentById(It.IsAny<Guid>())).ReturnsAsync(error);
            _storageClient.Setup(x => x.DownloadFileAsync(It.IsAny<string>())).ReturnsAsync(new MemoryStream());

            var handler = new GetWordFrequencyDocumentByIdQueryHandler(_storageClient.Object, _documentsService.Object, _logger.Object);

            // Act
            var result = await handler.Handle(query, CancellationToken.None);

            // Assert
            result.IsError.Should().BeTrue();
        }

    }
}
