﻿using DocumentAnalyzer.Application.Common.Behaviours;
using DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.Commands;
using DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.DTOs;
using DocumentAnalyzer.Domain.Entities;
using DocumentAnalyzer.Domain.Persistence;
using DocumentAnalyzer.Domain.Storage;
using ErrorOr;
using FluentAssertions;
using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;

namespace DocumentAnalyzer.Application.UnitTests.WordFrequencies.Commands
{
    public class WordFrequencyUploadCommandTests
    {
        private readonly Mock<IUnitOfWork> _unitOfWork;
        private readonly Mock<IStorageClient> _storageClient;
        private readonly Mock<IPublishEndpoint> _publishEndpoint;
        private readonly Mock<ILogger<WordFrequencyUploadCommandHandler>> _logger;

        public WordFrequencyUploadCommandTests()
        {
            _unitOfWork = new();
            _storageClient = new();
            _publishEndpoint = new();
            _logger = new();
        }

        [Fact]
        public async void Handle_Should_CallRepositoryAddAsync_WhenCommandIsCalled()
        {
            // Arrange
            var formFile = new Mock<IFormFile>();
            var command = new WordFrequencyUploadCommand { File = formFile.Object };
            _unitOfWork.Setup(x => x.Documents.AddAsync(It.IsAny<Document>())).Returns(Task.FromResult(new Document()));


            var handler = new WordFrequencyUploadCommandHandler(_storageClient.Object, _unitOfWork.Object, _publishEndpoint.Object, _logger.Object);

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert
            result.IsError.Should().BeFalse();
            _unitOfWork.Verify(x => x.Documents.AddAsync(It.IsAny<Document>()), Times.Once);
        }

        [Fact]
        public async Task Handle_Should_ReturnFailure_When_FileIsNotTextFile()
        {
            // Arrange
            var loggerMock = new Mock<ILogger<WordFrequencyUploadCommandHandler>>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var storageClientMock = new Mock<IStorageClient>();
            var publishEndpointMock = new Mock<IPublishEndpoint>();

            var formFileMock = new Mock<IFormFile>();
            formFileMock.Setup(x => x.Length).Returns(1000);
            formFileMock.Setup(x => x.ContentType).Returns("text/plain");
            formFileMock.Setup(x => x.FileName).Returns("image.jpg");

            var command = new WordFrequencyUploadCommand
            {
                File = formFileMock.Object
            };

            var validationBehavior = new ValidationBehaviour<WordFrequencyUploadCommand, ErrorOr<WordFrequencyUploadResponse>>(new WordFrequencyUploadCommandValidator());

            var handler = new WordFrequencyUploadCommandHandler(storageClientMock.Object, unitOfWorkMock.Object, publishEndpointMock.Object, loggerMock.Object);

            // Act
            var result = await validationBehavior.Handle(command, () => handler.Handle(command, default), default);

            // Assert
            result.IsError.Should().BeTrue();
            result.FirstError.Description.Should().Contain("The file must be a TEXT file");
        }

        [Fact]
        public async Task Handle_Should_ReturnFailure_When_FileContentTypeIsNotPlainText()
        {
            // Arrange
            var loggerMock = new Mock<ILogger<WordFrequencyUploadCommandHandler>>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var storageClientMock = new Mock<IStorageClient>();
            var publishEndpointMock = new Mock<IPublishEndpoint>();

            var formFileMock = new Mock<IFormFile>();
            formFileMock.Setup(x => x.Length).Returns(1000);
            formFileMock.Setup(x => x.FileName).Returns("image.txt");
            formFileMock.Setup(x => x.ContentType).Returns("image/jpeg");

            var command = new WordFrequencyUploadCommand
            {
                File = formFileMock.Object
            };

            var validationBehavior = new ValidationBehaviour<WordFrequencyUploadCommand, ErrorOr<WordFrequencyUploadResponse>>(new WordFrequencyUploadCommandValidator());

            var handler = new WordFrequencyUploadCommandHandler(storageClientMock.Object, unitOfWorkMock.Object, publishEndpointMock.Object, loggerMock.Object);

            // Act
            var result = await validationBehavior.Handle(command, () => handler.Handle(command, default), default);

            // Assert
            result.IsError.Should().BeTrue();
            result.FirstError.Description.Should().Contain("The file must be a plain text");
        }


        [Fact]
        public async Task Handle_Should_ReturnFailure_When_FileIsEmpty()
        {
            // Arrange
            var loggerMock = new Mock<ILogger<WordFrequencyUploadCommandHandler>>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var storageClientMock = new Mock<IStorageClient>();
            var publishEndpointMock = new Mock<IPublishEndpoint>();

            var formFileMock = new Mock<IFormFile>();
            formFileMock.Setup(x => x.FileName).Returns("test.txt");
            formFileMock.Setup(x => x.Length).Returns(0);

            var command = new WordFrequencyUploadCommand { File = formFileMock.Object };

            var validationBehavior = new ValidationBehaviour<WordFrequencyUploadCommand, ErrorOr<WordFrequencyUploadResponse>>(new WordFrequencyUploadCommandValidator());

            var handler = new WordFrequencyUploadCommandHandler(storageClientMock.Object, unitOfWorkMock.Object, publishEndpointMock.Object, loggerMock.Object);

            // Act
            var result = await validationBehavior.Handle(command, () => handler.Handle(command, default), default);

            // Assert
            result.IsError.Should().BeTrue();
        }
    }
}
