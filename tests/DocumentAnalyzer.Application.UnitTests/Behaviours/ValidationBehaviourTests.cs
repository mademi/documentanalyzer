﻿using DocumentAnalyzer.Application.Common.Behaviours;
using DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.Commands;
using DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.DTOs;
using ErrorOr;
using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Microsoft.AspNetCore.Http;
using Moq;
using System.Text;

namespace DocumentAnalyzer.Application.UnitTests.Behaviours
{
    public class ValidationBehaviourTests
    {
        private readonly Mock<RequestHandlerDelegate<ErrorOr<WordFrequencyUploadResponse>>> _mockNextBehavior;
        private readonly Mock<IValidator<WordFrequencyUploadCommand>> _mockValidator;
        private readonly ValidationBehaviour<WordFrequencyUploadCommand, ErrorOr<WordFrequencyUploadResponse>> _validationBehavior;

        public ValidationBehaviourTests()
        {
            _mockNextBehavior = new Mock<RequestHandlerDelegate<ErrorOr<WordFrequencyUploadResponse>>>();
            _mockValidator = new Mock<IValidator<WordFrequencyUploadCommand>>();
            _validationBehavior = new ValidationBehaviour<WordFrequencyUploadCommand, ErrorOr<WordFrequencyUploadResponse>>(_mockValidator.Object);
        }

        [Fact]
        public async Task InvokeValidationBehavior_WhenValidatorResultIsValid_ShouldInvokeNextBehavior()
        {
            // Arrange
            var formFile = CreateTestFormFile("hello unit test", "test.txt");
            var command = new WordFrequencyUploadCommand { File = formFile };
            var response = new WordFrequencyUploadResponse(Guid.NewGuid());


            _mockValidator.Setup(x => x.ValidateAsync(command, CancellationToken.None))
                          .ReturnsAsync(new ValidationResult());

            _mockNextBehavior.Setup(x => x.Invoke()).ReturnsAsync(response);

            // Act
            var result = await _validationBehavior.Handle(command, _mockNextBehavior.Object, default);

            // Assert
            result.IsError.Should().BeFalse();
            result.Value.Should().BeEquivalentTo(response);
        }

        [Fact]
        public async Task InvokeValidationBehavior_WhenValidatorResultIsNotValid_ShouldReturnListOfErrors()
        {
            // Arrange
            var formFile = CreateTestFormFile("hello unit test", "test.txt");
            var command = new WordFrequencyUploadCommand { File = formFile };
            List<ValidationFailure> validationFailures = [new(propertyName: "File", errorMessage: "Bad File")];

            _mockValidator.Setup(x => x.ValidateAsync(command, It.IsAny<CancellationToken>()))
                          .ReturnsAsync(new ValidationResult(validationFailures));

            // Act
            var result = await _validationBehavior.Handle(command, _mockNextBehavior.Object, default);

            // Assert
            result.IsError.Should().BeTrue();
            result.FirstError.Code.Should().Be("File");
            result.FirstError.Description.Should().Be("Bad File");
        }

        [Fact]
        public async Task InvokeValidationBehavior_WhenNoValidator_ShouldInvokeNextBehavior()
        {
            // Arrange
            var formFile = CreateTestFormFile("hello unit test", "test.txt");
            var command = new WordFrequencyUploadCommand { File = formFile };
            var response = new WordFrequencyUploadResponse(Guid.NewGuid());
            var validationBehavior = new ValidationBehaviour<WordFrequencyUploadCommand, ErrorOr<WordFrequencyUploadResponse>>();

            _mockNextBehavior.Setup(x => x.Invoke()).ReturnsAsync(response);

            // Act
            var result = await validationBehavior.Handle(command, _mockNextBehavior.Object, default);

            // Assert
            result.IsError.Should().BeFalse();
            result.Value.Should().Be(response);
        }

        private IFormFile CreateTestFormFile(string content, string fileName)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(content);

            return new FormFile(
                baseStream: new MemoryStream(bytes),
                baseStreamOffset: 0,
                length: bytes.Length,
                name: "Data",
                fileName: fileName
            );
        }


    }
}
