﻿using DocumentAnalyzer.Consumer.Parsers;
using DocumentAnalyzer.Consumer.Tokenizer;
using FluentAssertions;
using Moq;

namespace DocumentAnalyzer.Consumer.UnitTests.Parsers
{
    public class WordFrequencyParserTests
    {
        [Fact]
        public void UpdateWordFrequencies_ShouldIncrementFrequencies_WhenWordsAreRepeated()
        {
            // Arrange
            var mockTokenizer = new Mock<ITokenizer>();
            mockTokenizer.Setup(t => t.Tokenize("hello world hello")).Returns(["hello", "world", "hello"]);
            var parser = new WordFrequencyParser(mockTokenizer.Object);

            // Act
            parser.UpdateWordFrequencies("hello world hello");

            // Assert
            var result = parser.GetSortedFrequencies().ToList();
            result.First().Value.Should().Be(2);
            result.First().Key.Should().Be("hello");
            result.Last().Value.Should().Be(1);
            result.Last().Key.Should().Be("world");
        }

        [Fact]
        public void GetSortedFrequencies_ShouldReturnFrequenciesInDescendingOrder_WhenValidInput()
        {
            // Arrange
            var mockTokenizer = new Mock<ITokenizer>();
            mockTokenizer.Setup(t => t.Tokenize("one two two three three three")).Returns(["one", "two", "two", "three", "three", "three"]);

            var parser = new WordFrequencyParser(mockTokenizer.Object);
            parser.UpdateWordFrequencies("one two two three three three");

            // Act
            var result = parser.GetSortedFrequencies().ToList();

            // Assert
            result[0].Value.Should().Be(3);
            result[0].Key.Should().Be("three");

            result[1].Value.Should().Be(2);
            result[1].Key.Should().Be("two");

            result[2].Value.Should().Be(1);
            result[2].Key.Should().Be("one");
        }

    }
}
