﻿using DocumentAnalyzer.Consumer.Tokenizer;
using FluentAssertions;

namespace DocumentAnalyzer.Consumer.UnitTests.Tokenizers
{
    public class RegexTokenizerTests
    {
        [Fact]
        public void TokenizerTokenize_ShouldReturnCorrectTokens_WhenInputIsValid()
        {
            // Arrange
            var tokenizer = new RegexTokenizer();

            // Act
            var result = tokenizer.Tokenize("Testing, Hello, World").ToList();

            // Assert
            result.Should().Contain("testing");
            result.Should().Contain("hello");
            result.Should().Contain("world");
            result.Should().HaveCount(3);
        }

        [Theory]
        [InlineData("hej1, 2, 3")]
        [InlineData("!@# $%^ &* ()")]
        [InlineData("123 456 789")]
        [InlineData("")]
        [InlineData("     ")]
        [InlineData("1a 2b 3c")]
        public void TokenizerTokenize_ShouldReturnZeroTokens_WhenInputIsInvalid(string input)
        {
            // Arrange
            var tokenizer = new RegexTokenizer();

            // Act
            var result = tokenizer.Tokenize(input).ToList();

            // Assert
            result.Should().HaveCount(0);
        }
    }
}
