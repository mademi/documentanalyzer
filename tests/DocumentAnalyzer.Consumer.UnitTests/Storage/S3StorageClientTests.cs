﻿using Amazon.S3;
using Amazon.S3.Model;
using DocumentAnalyzer.Consumer.Configurations;
using DocumentAnalyzer.Consumer.Storage;
using Microsoft.Extensions.Options;
using Moq;

namespace DocumentAnalyzer.Consumer.UnitTests.Storage
{
    public class S3StorageClientTests
    {
        [Fact]
        public async Task UploadFileAsync_UploadsContentCorrectly_WhenInputIsValid()
        {
            var mockS3Client = new Mock<IAmazonS3>();
            var mockOptions = new Mock<IOptions<StorageConfigurations>>();
            mockOptions.Setup(o => o.Value).Returns(new StorageConfigurations
            {
                BucketName = "test-bucket",
                Endpoint = "http://example.com",
                AccessKey = "key",
                SecretKey = "secret"
            });

            var client = new S3StorageClient(mockOptions.Object, mockS3Client.Object);

            string content = "Hello, world!";
            string objectName = "test.txt";

            await client.UploadFileAsync(content, objectName);

            mockS3Client.Verify(s => s.PutObjectAsync(It.Is<PutObjectRequest>(
                p => p.BucketName == "test-bucket" &&
                     p.Key == objectName &&
                     p.ContentType == "text/plain"
            ), It.IsAny<CancellationToken>()), Times.Once());
        }

    }
}
