DOCKER_FILE = docker-compose.yml

.PHONY: start stop restart

start: ## Start the project with all dependencies
	docker-compose -f $(DOCKER_FILE) up -d

stop: ## Stop the project and all dependencies
	docker-compose -f $(DOCKER_FILE) down

restart: ## Restart the project and all dependencies
	docker-compose -f $(DOCKER_FILE) down
	docker-compose -f $(DOCKER_FILE) up -d

logs: ## Show the logs for all containers 
	docker-compose logs --tail=200

status: ## Show status of containers
	docker-compose ps