using Amazon.S3;
using DocumentAnalyzer.Consumer.Configurations;
using DocumentAnalyzer.Consumer.Consumers;
using DocumentAnalyzer.Consumer.Storage;
using DocumentAnalyzer.Consumer.Tokenizer;
using MassTransit;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<StorageConfigurations>(builder.Configuration.GetSection("StorageConfigurations"));

builder.Services.AddSingleton<IAmazonS3>(serviceProvider =>
{
    var storageConfigurations = serviceProvider.GetRequiredService<IOptions<StorageConfigurations>>().Value;
    var config = new AmazonS3Config
    {
        ServiceURL = storageConfigurations.Endpoint,
        ForcePathStyle = true,
        UseHttp = true
    };

    return new AmazonS3Client(storageConfigurations.AccessKey, storageConfigurations.SecretKey, config);
});


builder.Services.AddSingleton<IStorageClient, S3StorageClient>();

builder.Services.AddSingleton<ITokenizer, RegexTokenizer>();

builder.Services.AddMassTransit(options =>
{
    options.SetKebabCaseEndpointNameFormatter();

    options.AddConsumer<DocumentCreatedConsumer>();

    options.UsingRabbitMq((context, configurator) =>
    {
        configurator.Host(new Uri(builder.Configuration["Messaging:Host"]!), x =>
        {
            x.Username(builder.Configuration["Messaging:Username"]);
            x.Password(builder.Configuration["Messaging:Password"]);
        });

        configurator.ConfigureEndpoints(context);
    });
});


// Add services to the container.

var app = builder.Build();


// Configure the HTTP request pipeline.

app.Run();
