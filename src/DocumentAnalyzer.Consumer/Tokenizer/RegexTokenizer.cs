﻿using System.Text.RegularExpressions;

namespace DocumentAnalyzer.Consumer.Tokenizer
{
    public class RegexTokenizer : ITokenizer
    {
        public IEnumerable<string> Tokenize(string content)
        {
            return Regex.Matches(content.ToLower(), @"\b[a-z]+\b")
                        .Cast<Match>()
                        .Select(match => match.Value);
        }
    }

}
