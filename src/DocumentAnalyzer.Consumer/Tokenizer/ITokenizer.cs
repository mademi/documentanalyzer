﻿namespace DocumentAnalyzer.Consumer.Tokenizer
{
    public interface ITokenizer
    {
        IEnumerable<string> Tokenize(string content);
    }
}
