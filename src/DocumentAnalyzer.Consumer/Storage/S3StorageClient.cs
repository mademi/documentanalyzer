﻿using Amazon.S3;
using Amazon.S3.Model;
using DocumentAnalyzer.Consumer.Configurations;
using Microsoft.Extensions.Options;

namespace DocumentAnalyzer.Consumer.Storage
{
    public class S3StorageClient : IStorageClient
    {
        private readonly IAmazonS3 _client;
        private readonly StorageConfigurations _storageConfigurations;

        public S3StorageClient(IOptions<StorageConfigurations> configurations, IAmazonS3 client)
        {
            _storageConfigurations = configurations.Value ?? throw new ArgumentNullException(nameof(configurations));
            _client = client;
        }

        public async Task UploadFileAsync(string content, string objectName)
        {
            using var memoryStream = new MemoryStream();
            using var streamWriter = new StreamWriter(memoryStream);
            streamWriter.Write(content);
            streamWriter.Flush();
            memoryStream.Position = 0;

            var putRequest = new PutObjectRequest
            {
                InputStream = memoryStream,
                BucketName = _storageConfigurations.BucketName,
                Key = objectName,
                ContentType = "text/plain",
                AutoCloseStream = false
            };

            await _client.PutObjectAsync(putRequest);

        }

        public async Task<Stream> DownloadFileAsync(string objectName)
        {
            var getRequest = new GetObjectRequest
            {
                BucketName = _storageConfigurations.BucketName,
                Key = objectName
            };

            var response = await _client.GetObjectAsync(getRequest);
            return response.ResponseStream;
        }
    }
}
