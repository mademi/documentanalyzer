﻿
namespace DocumentAnalyzer.Consumer.Storage
{
    public interface IStorageClient
    {
        Task<Stream> DownloadFileAsync(string objectName);
        Task UploadFileAsync(string content, string objectName);
    }
}