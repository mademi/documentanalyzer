﻿using DocumentAnalyzer.Consumer.Parsers;
using DocumentAnalyzer.Consumer.Storage;
using DocumentAnalyzer.Consumer.Tokenizer;
using DocumentAnalyzer.Contracts.Documents.Events;
using MassTransit;

namespace DocumentAnalyzer.Consumer.Consumers
{
    public class DocumentCreatedConsumer : IConsumer<DocumentCreatedEvent>
    {
        private readonly IStorageClient _storageClient;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly ITokenizer _tokenizer;

        public DocumentCreatedConsumer(IStorageClient storageClient, IPublishEndpoint publishEndpoint, ITokenizer tokenizer)
        {
            _storageClient = storageClient ?? throw new ArgumentNullException(nameof(storageClient));
            _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
            _tokenizer = tokenizer ?? throw new ArgumentNullException(nameof(tokenizer));
        }

        public async Task Consume(ConsumeContext<DocumentCreatedEvent> context)
        {
            try
            {
                var fileId = Guid.NewGuid();
                var wordFrequencyParser = new WordFrequencyParser(_tokenizer);

                await ParseDocument(wordFrequencyParser, context.Message.OriginalFileKey);

                var content = FormattedFrequenciesToString(wordFrequencyParser.GetSortedFrequencies());

                await _storageClient.UploadFileAsync(content, fileId.ToString());

                await _publishEndpoint.Publish(new DocumentProcessedEvent
                {
                    Id = context.Message.Id,
                    IsProcessed = true,
                    WordFrequencyFileKey = fileId.ToString(),
                });
            }
            catch (Exception ex)
            {
                // Logging and handle exception
            }
        }

        private async Task ParseDocument(WordFrequencyParser parser, string fileKey)
        {
            using var fileStream = await _storageClient.DownloadFileAsync(fileKey);
            using var reader = new StreamReader(fileStream);

            string line;
            while ((line = await reader.ReadLineAsync()) != null)
            {
                parser.UpdateWordFrequencies(line);
            }
        }

        private static string FormattedFrequenciesToString(IEnumerable<KeyValuePair<string, int>> frequencies)
        {
            return string.Join("\n", frequencies.Select(f => $"{f.Key}: {f.Value}"));
        }

    }
}
