﻿using DocumentAnalyzer.Consumer.Tokenizer;
using System.Collections.Concurrent;

namespace DocumentAnalyzer.Consumer.Parsers
{
    public class WordFrequencyParser : IFrequencyParser
    {
        private readonly ITokenizer _tokenizer;
        private readonly ConcurrentDictionary<string, int> _wordFrequencies = new();

        public WordFrequencyParser(ITokenizer tokenizer)
        {
            _tokenizer = tokenizer;
        }

        public void UpdateWordFrequencies(string inputLine)
        {
            var words = _tokenizer.Tokenize(inputLine);
            foreach (var word in words)
            {
                _wordFrequencies.AddOrUpdate(word, 1, (key, value) => value + 1);
            }
        }

        public IOrderedEnumerable<KeyValuePair<string, int>> GetSortedFrequencies()
        {
            return _wordFrequencies.OrderByDescending(pair => pair.Value);
        }
    }

}
