﻿
namespace DocumentAnalyzer.Consumer.Parsers
{
    public interface IFrequencyParser
    {
        IOrderedEnumerable<KeyValuePair<string, int>> GetSortedFrequencies();
        void UpdateWordFrequencies(string inputLine);
    }
}