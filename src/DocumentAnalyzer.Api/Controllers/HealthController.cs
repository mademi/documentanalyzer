﻿using DocumentAnalyzer.Contracts.Health.Response;
using Microsoft.AspNetCore.Mvc;

namespace DocumentAnalyzer.Api.Controllers
{
    [ApiController]
    [Route("v1/health")]
    public class HealthController : ControllerBase
    {
        /// <summary>
        /// Provides a interface to check the health of the TextAnalyzer service
        /// </summary>
        /// <returns>An ActionResult containing the health status of the analyzer service</returns>
        [HttpGet]
        public ActionResult<HealthResponse> GetHealthProbe()
        {
            return Ok(new HealthResponse("Healthy"));
        }
    }
}
