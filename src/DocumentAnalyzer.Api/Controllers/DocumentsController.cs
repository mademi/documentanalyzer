﻿using DocumentAnalyzer.Api.Common;
using DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.Commands;
using DocumentAnalyzer.Application.WordFrequencies.GetOriginalDocumentById.Queries;
using DocumentAnalyzer.Application.WordFrequencies.GetWordFrequencyDocumentById.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;


namespace DocumentAnalyzer.Api.Controllers
{
    [ApiController]
    [Route("v1/documents")]
    public class DocumentsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DocumentsController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        /// <summary>
        /// Creates a document that triggers a word frequency job 
        /// </summary>
        /// <param name="command">Text file that you wish to upload</param>
        /// <returns>201 CreatedAt w/ location header and response body containing the document ID</returns>
        [HttpPost("word-frequency")]
        public async Task<IActionResult> CreateWordFrequencyDocumentJob(WordFrequencyUploadCommand command)
        {
            var result = await _mediator.Send(command);

            return result.Match<IActionResult>(
                response => CreatedAtAction(
                    actionName: nameof(DownloadDocumentWordFrequencySummaryById),
                    routeValues: new { result.Value.Id },
                    value: response.Id),
                 errors => new BadRequestObjectResult(new { Errors = errors }));
        }

        /// <summary>
        /// Provides an interface for the end user to download the word frequency summary report
        /// </summary>
        /// <param name="id">Identifier of the document ID</param>
        /// <returns></returns>
        [HttpGet("word-frequency/{id:guid}")]
        public async Task<IActionResult> DownloadDocumentWordFrequencySummaryById(Guid id)
        {
            var query = new GetWordFrequencyDocumentByIdQuery(id);

            var response = await _mediator.Send(query);

            if (response.IsError) return NotFound();

            var fileName = $"{id}.txt";

            return File(response.Value, ContentTypes.OctetStream, fileName);
        }

        /// <summary>
        /// Provides an interface for the end user to download the original document (untouched)
        /// </summary>
        /// <param name="id">Identifier of the document ID</param>
        /// <returns>File stream</returns>
        [HttpGet("word-frequency/{id:guid}/original")]
        public async Task<IActionResult> DownloadOriginalDocument(Guid id)
        {
            var query = new GetOriginDocumentWFQuery(id);

            var response = await _mediator.Send(query);

            if (response.IsError) return NotFound();

            var fileName = $"{id}_original.txt";

            return File(response.Value, ContentTypes.OctetStream, fileName);
        }
    }
}
