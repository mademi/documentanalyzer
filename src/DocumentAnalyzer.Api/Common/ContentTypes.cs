﻿namespace DocumentAnalyzer.Api.Common
{
    public static class ContentTypes
    {
        public const string Text = "text/plain";
        public const string OctetStream = "application/octet-stream";
    }
}
