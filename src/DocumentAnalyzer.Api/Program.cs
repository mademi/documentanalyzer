using DocumentAnalyzer.Api;
using DocumentAnalyzer.Application;
using DocumentAnalyzer.Infrastructure;
using DocumentAnalyzer.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Prometheus;
using Serilog;

var builder = WebApplication.CreateBuilder(args);
{

    // Hardcoded logging configuration, for dev purposes
    // Intended to be replaced with environment variables / app settings

    var logger = new LoggerConfiguration()
    .WriteTo.Console()
    .Enrich.FromLogContext()
    .CreateLogger();


    builder.Host.UseSerilog(logger);

    builder.Services
        .AddApplication(builder.Configuration)
        .AddInfrastructure(builder.Configuration)
        .AddPresentation()
        .AddControllers();


    // Code-first migrations, for dev purposes
    // Not suitable in production
    try
    {
        using IServiceScope scope = builder.Services.BuildServiceProvider().CreateScope();
        var db = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
        db.Database.Migrate();
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }

    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
}


var app = builder.Build();
{
    app.UseMetricServer();

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpMetrics();

    app.UseSerilogRequestLogging();

    app.UseExceptionHandler();

    app.MapControllers();

    app.Run();
}

public partial class Program { }

