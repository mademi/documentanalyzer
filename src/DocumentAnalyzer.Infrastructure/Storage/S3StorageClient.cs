﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Util;
using DocumentAnalyzer.Domain.Storage;
using DocumentAnalyzer.Infrastructure.Configurations;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DocumentAnalyzer.Infrastructure.Storage
{
    public class S3StorageClient : IStorageClient
    {
        private readonly IAmazonS3 _client;
        private readonly StorageConfigurations _storageConfigurations;
        private ILogger<S3StorageClient> _logger;

        public S3StorageClient(IOptions<StorageConfigurations> configurations, IAmazonS3 client, ILogger<S3StorageClient> logger)
        {
            _storageConfigurations = configurations.Value ?? throw new ArgumentNullException(nameof(configurations));
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task UploadFileAsync(IFormFile formFile, string objectName)
        {
            _logger.LogInformation("S3StorageClient::UploadFileAsync called");

            // Ensure bucket exists
            await EnsureBucketExistsAsync(_storageConfigurations.BucketName);

            using var stream = formFile.OpenReadStream();

            var putRequest = new PutObjectRequest
            {
                InputStream = stream,
                BucketName = _storageConfigurations.BucketName,
                Key = objectName,
                ContentType = formFile.ContentType,
                AutoCloseStream = false
            };

            await _client.PutObjectAsync(putRequest);
        }

        public async Task<Stream> DownloadFileAsync(string objectName)
        {
            _logger.LogInformation("S3StorageClient::DownloadFileAsync called");

            var getRequest = new GetObjectRequest
            {
                BucketName = _storageConfigurations.BucketName,
                Key = objectName
            };

            var response = await _client.GetObjectAsync(getRequest);
            return response.ResponseStream;
        }

        public async Task DeleteFileAsync(string objectName)
        {
            _logger.LogInformation("S3StorageClient::DeleteFileAsync called");

            var deleteRequest = new DeleteObjectRequest
            {
                BucketName = _storageConfigurations.BucketName,
                Key = objectName
            };

            await _client.DeleteObjectAsync(deleteRequest);
        }

        private async Task EnsureBucketExistsAsync(string bucketName)
        {
            _logger.LogInformation("S3StorageClient::EnsureBucketExistsAsync called");

            var exists = await AmazonS3Util.DoesS3BucketExistV2Async(_client, bucketName);

            if (!exists)
            {
                var request = new PutBucketRequest
                {
                    BucketName = bucketName
                };

                await _client.PutBucketAsync(request);
            }

        }
    }
}
