﻿using DocumentAnalyzer.Application.Services;
using DocumentAnalyzer.Contracts.Documents.Events;
using DocumentAnalyzer.Domain.Entities;
using DocumentAnalyzer.Domain.Persistence;
using ErrorOr;
using Microsoft.Extensions.Logging;

namespace DocumentAnalyzer.Infrastructure.Services
{
    public class DocumentsService : IDocumentsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<DocumentsService> _logger;

        public DocumentsService(IUnitOfWork unitOfWork, ILogger<DocumentsService> logger)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<ErrorOr<Document>> AddProcessedDocumentMetadataAsync(Guid documentId, DocumentProcessedEvent documentProcessedEvent)
        {
            _logger.LogInformation("DocumentsService::AddProcessedDocumentMetadataAsync called");

            var document = await _unitOfWork.Documents.GetByIdAsync(documentId);

            if (document == null) return Error.NotFound();

            document.WordFrequencyFileKey = documentProcessedEvent.WordFrequencyFileKey;
            document.IsProcessed = true;

            await _unitOfWork.CommitAsync(CancellationToken.None);

            return document;
        }

        public async Task<ErrorOr<Document>> GetDocumentById(Guid documentId)
        {
            _logger.LogInformation("DocumentsService::GetDocumentById called");

            var document = await _unitOfWork.Documents.GetByIdAsync(documentId);

            if (document == null) return Error.NotFound();

            return document;
        }
    }
}
