﻿using DocumentAnalyzer.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DocumentAnalyzer.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Document> Documents { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            base.OnConfiguring(options);
        }

    }

}
