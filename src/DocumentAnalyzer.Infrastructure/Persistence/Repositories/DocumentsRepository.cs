﻿using DocumentAnalyzer.Domain.Entities;
using DocumentAnalyzer.Domain.Persistence.Repositories;

namespace DocumentAnalyzer.Infrastructure.Persistence.Repositories
{
    public class DocumentsRepository : Repository<Document>, IDocumentsRepository
    {
        public DocumentsRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
