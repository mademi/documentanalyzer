﻿using DocumentAnalyzer.Domain.Persistence;
using DocumentAnalyzer.Domain.Persistence.Repositories;
using DocumentAnalyzer.Infrastructure.Persistence.Repositories;

namespace DocumentAnalyzer.Infrastructure.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        public IDocumentsRepository Documents { get; private set; }

        public UnitOfWork(ApplicationDbContext applicationDbContext)
        {
            _context = applicationDbContext ?? throw new ArgumentNullException(nameof(applicationDbContext));

            Documents = new DocumentsRepository(applicationDbContext);
        }

        public async Task<int> CommitAsync(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context?.Dispose();
            }
        }
    }

}
