﻿using Amazon.S3;
using DocumentAnalyzer.Application.Services;
using DocumentAnalyzer.Domain.Persistence;
using DocumentAnalyzer.Domain.Storage;
using DocumentAnalyzer.Infrastructure.Configurations;
using DocumentAnalyzer.Infrastructure.Persistence;
using DocumentAnalyzer.Infrastructure.Services;
using DocumentAnalyzer.Infrastructure.Storage;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;


namespace DocumentAnalyzer.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            var assembly = typeof(DependencyInjection).Assembly;

            services
                .AddServices(configuration)
                .AddStorageService(configuration)
                .AddPersistence(configuration);

            return services;
        }

        private static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IDocumentsService, DocumentsService>();

            return services;
        }

        private static IServiceCollection AddStorageService(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<StorageConfigurations>(configuration.GetSection("StorageConfigurations"));

            services.AddSingleton<IAmazonS3>(serviceProvider =>
            {
                var storageConfigurations = serviceProvider.GetRequiredService<IOptions<StorageConfigurations>>().Value;
                var config = new AmazonS3Config
                {
                    ServiceURL = storageConfigurations.Endpoint,
                    ForcePathStyle = true,
                    UseHttp = true
                };

                return new AmazonS3Client(storageConfigurations.AccessKey, storageConfigurations.SecretKey, config);
            });

            services.AddSingleton<IStorageClient, S3StorageClient>();

            return services;
        }

        private static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
