﻿using DocumentAnalyzer.Application.Common.Behaviours;
using DocumentAnalyzer.Application.Consumers.Documents;
using FluentValidation;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DocumentAnalyzer.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            var assembly = typeof(DependencyInjection).Assembly;

            services.AddMediatR(options =>
            {
                options.RegisterServicesFromAssembly(assembly);
                options.AddOpenBehavior(typeof(ValidationBehaviour<,>));
            });

            services.AddMassTransit(options =>
            {
                options.SetKebabCaseEndpointNameFormatter();

                options.AddConsumer<DocumentProcessedEventConsumer>();

                options.UsingRabbitMq((context, configurator) =>
                {
                    configurator.Host(new Uri(configuration["Messaging:Host"]!), x =>
                    {
                        x.Username(configuration["Messaging:Username"]);
                        x.Password(configuration["Messaging:Password"]);
                    });

                    configurator.ConfigureEndpoints(context);
                });
            });

            services.AddValidatorsFromAssembly(assembly);

            return services;
        }
    }
}
