﻿using DocumentAnalyzer.Application.Services;
using DocumentAnalyzer.Contracts.Documents.Events;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace DocumentAnalyzer.Application.Consumers.Documents
{
    public class DocumentProcessedEventConsumer : IConsumer<DocumentProcessedEvent>
    {
        private readonly IDocumentsService _documentsService;
        private readonly ILogger<DocumentProcessedEventConsumer> _logger;

        public DocumentProcessedEventConsumer(IDocumentsService documentsService, ILogger<DocumentProcessedEventConsumer> logger)
        {
            _documentsService = documentsService ?? throw new ArgumentNullException(nameof(documentsService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Consume(ConsumeContext<DocumentProcessedEvent> context)
        {
            _logger.LogInformation("DocumentProcessedEventConsumer::Consume called");
            var message = context.Message;
            await _documentsService.AddProcessedDocumentMetadataAsync(message.Id, message);
        }
    }
}
