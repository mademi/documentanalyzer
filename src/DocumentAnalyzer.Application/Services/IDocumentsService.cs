﻿using DocumentAnalyzer.Contracts.Documents.Events;
using DocumentAnalyzer.Domain.Entities;
using ErrorOr;

namespace DocumentAnalyzer.Application.Services
{
    public interface IDocumentsService
    {
        Task<ErrorOr<Document>> GetDocumentById(Guid documentId);
        Task<ErrorOr<Document>> AddProcessedDocumentMetadataAsync(Guid documentId, DocumentProcessedEvent documentProcessedEvent);
    }
}
