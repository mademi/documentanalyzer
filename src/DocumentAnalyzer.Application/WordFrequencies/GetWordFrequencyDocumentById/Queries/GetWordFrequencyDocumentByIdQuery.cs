﻿using ErrorOr;
using MediatR;

namespace DocumentAnalyzer.Application.WordFrequencies.GetWordFrequencyDocumentById.Queries
{
    public record GetWordFrequencyDocumentByIdQuery(Guid Id) : IRequest<ErrorOr<Stream>>;
}
