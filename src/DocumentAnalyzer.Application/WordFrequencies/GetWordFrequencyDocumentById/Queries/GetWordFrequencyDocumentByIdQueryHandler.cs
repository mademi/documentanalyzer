﻿using DocumentAnalyzer.Application.Services;
using DocumentAnalyzer.Domain.Storage;
using ErrorOr;
using MediatR;
using Microsoft.Extensions.Logging;

namespace DocumentAnalyzer.Application.WordFrequencies.GetWordFrequencyDocumentById.Queries
{
    public class GetWordFrequencyDocumentByIdQueryHandler : IRequestHandler<GetWordFrequencyDocumentByIdQuery, ErrorOr<Stream>>
    {
        private readonly IStorageClient _storageClient;
        private readonly IDocumentsService _documentsService;
        private readonly ILogger<GetWordFrequencyDocumentByIdQueryHandler> _logger;

        public GetWordFrequencyDocumentByIdQueryHandler(IStorageClient storageClient, IDocumentsService documentsService, ILogger<GetWordFrequencyDocumentByIdQueryHandler> logger)
        {
            _storageClient = storageClient ?? throw new ArgumentNullException(nameof(storageClient));
            _documentsService = documentsService ?? throw new ArgumentNullException(nameof(documentsService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<ErrorOr<Stream>> Handle(GetWordFrequencyDocumentByIdQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetWordFrequencyDocumentByIdQueryHandler::Handle called");

            var document = await _documentsService.GetDocumentById(request.Id);

            if (document.IsError || !document.Value.IsProcessed) return Error.NotFound();

            return await _storageClient.DownloadFileAsync(document.Value.WordFrequencyFileKey!);
        }
    }
}
