﻿using DocumentAnalyzer.Application.Services;
using DocumentAnalyzer.Domain.Storage;
using ErrorOr;
using MediatR;
using Microsoft.Extensions.Logging;

namespace DocumentAnalyzer.Application.WordFrequencies.GetOriginalDocumentById.Queries
{
    public class GetOriginDocumentWFQueryHandler : IRequestHandler<GetOriginDocumentWFQuery, ErrorOr<Stream>>
    {
        private readonly IStorageClient _storageClient;
        private readonly IDocumentsService _documentsService;
        private readonly ILogger<GetOriginDocumentWFQueryHandler> _logger;

        public GetOriginDocumentWFQueryHandler(IStorageClient storageClient, IDocumentsService documentsService, ILogger<GetOriginDocumentWFQueryHandler> logger)
        {
            _storageClient = storageClient ?? throw new ArgumentNullException(nameof(storageClient));
            _documentsService = documentsService ?? throw new ArgumentNullException(nameof(documentsService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<ErrorOr<Stream>> Handle(GetOriginDocumentWFQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetOriginDocumentWFQueryHandler::Handle called");

            var document = await _documentsService.GetDocumentById(request.Id);

            if (document.IsError) return Error.NotFound();

            return await _storageClient.DownloadFileAsync(document.Value.OriginalFileKey);
        }
    }
}
