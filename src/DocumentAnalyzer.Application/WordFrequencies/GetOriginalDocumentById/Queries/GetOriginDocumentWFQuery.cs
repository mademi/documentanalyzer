﻿using ErrorOr;
using MediatR;

namespace DocumentAnalyzer.Application.WordFrequencies.GetOriginalDocumentById.Queries
{
    public record GetOriginDocumentWFQuery(Guid Id) : IRequest<ErrorOr<Stream>>;
}
