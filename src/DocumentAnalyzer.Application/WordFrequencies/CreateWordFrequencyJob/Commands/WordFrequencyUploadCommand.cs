﻿using DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.DTOs;
using ErrorOr;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.Commands
{
    public class WordFrequencyUploadCommand : IRequest<ErrorOr<WordFrequencyUploadResponse>>
    {
        public required IFormFile File { get; set; }
    }
}