﻿using FluentValidation;

namespace DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.Commands
{
    public class WordFrequencyUploadCommandValidator : AbstractValidator<WordFrequencyUploadCommand>
    {
        public WordFrequencyUploadCommandValidator()
        {
            RuleFor(x => x.File)
            .NotNull()
            .WithMessage("A file is required.");

            RuleFor(x => x.File.Length)
            .GreaterThan(0)
            .WithMessage("The file cannot be empty.")
            .When(x => x.File != null);

            RuleFor(x => x.File.ContentType)
            .Equal("text/plain")
            .WithMessage("The file must be a plain text file.")
            .When(x => x.File != null);

            RuleFor(x => x.File.FileName)
            .Must(HaveTxtExtension)
            .WithMessage("The file must be a TEXT file and the extension must be .txt.")
            .When(x => x.File != null);
        }

        private bool HaveTxtExtension(string fileName)
        {
            return Path.GetExtension(fileName).Equals(".txt", StringComparison.CurrentCultureIgnoreCase);
        }

    }
}
