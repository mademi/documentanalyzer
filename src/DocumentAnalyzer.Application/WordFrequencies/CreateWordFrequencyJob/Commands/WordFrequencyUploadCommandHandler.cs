﻿using DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.DTOs;
using DocumentAnalyzer.Contracts.Documents.Events;
using DocumentAnalyzer.Domain.Entities;
using DocumentAnalyzer.Domain.Persistence;
using DocumentAnalyzer.Domain.Storage;
using ErrorOr;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;

namespace DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.Commands
{
    public class WordFrequencyUploadCommandHandler : IRequestHandler<WordFrequencyUploadCommand, ErrorOr<WordFrequencyUploadResponse>>
    {
        private readonly IStorageClient _storageClient;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly ILogger<WordFrequencyUploadCommandHandler> _logger;
        private const string _uploadBucket = "documents";

        public WordFrequencyUploadCommandHandler(IStorageClient storageClient, IUnitOfWork unitOfWork,
            IPublishEndpoint publishEndpoint, ILogger<WordFrequencyUploadCommandHandler> logger
            )
        {
            _storageClient = storageClient ?? throw new ArgumentNullException(nameof(storageClient));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<ErrorOr<WordFrequencyUploadResponse>> Handle(WordFrequencyUploadCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("WordFrequencyUploadCommandHandler::Handle called");

            var fileId = Guid.NewGuid();
            var document = new Document
            {
                BucketName = _uploadBucket,
                OriginalFileName = request.File.FileName,
                MimeType = request.File.Name,
                ContentType = request.File.ContentType,
                CreatedDate = DateTime.UtcNow,
                OriginalFileKey = fileId.ToString(),
            };

            await _unitOfWork.Documents.AddAsync(document);

            await _storageClient.UploadFileAsync(request.File, fileId.ToString());

            await _unitOfWork.CommitAsync(cancellationToken);

            await _publishEndpoint.Publish(new DocumentCreatedEvent
            {
                Id = document.Id,
                BucketName = _uploadBucket,
                OriginalFileKey = fileId.ToString()
            });

            return new WordFrequencyUploadResponse(document.Id);
        }
    }
}
