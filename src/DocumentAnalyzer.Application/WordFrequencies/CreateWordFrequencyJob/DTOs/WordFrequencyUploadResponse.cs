﻿namespace DocumentAnalyzer.Application.WordFrequencies.CreateWordFrequencyJob.DTOs
{
    public record WordFrequencyUploadResponse(Guid Id);
}
