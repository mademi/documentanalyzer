﻿using DocumentAnalyzer.Domain.Entities;

namespace DocumentAnalyzer.Domain.Persistence.Repositories
{
    public interface IDocumentsRepository : IRepository<Document>
    {
    }
}
