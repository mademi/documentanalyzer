﻿using DocumentAnalyzer.Domain.Common;
using System.Linq.Expressions;

namespace DocumentAnalyzer.Domain.Persistence.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<T?> GetByIdAsync(Guid id);
        Task<T?> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate);
        Task<T> AddAsync(T entity);
        void Update(T entity);
        void Remove(T entity);
        Task<IEnumerable<T>> GetAllAsync();
    }
}
