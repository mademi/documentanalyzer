﻿using DocumentAnalyzer.Domain.Persistence.Repositories;

namespace DocumentAnalyzer.Domain.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        IDocumentsRepository Documents { get; }

        Task<int> CommitAsync(CancellationToken cancellationToken);
    }
}
