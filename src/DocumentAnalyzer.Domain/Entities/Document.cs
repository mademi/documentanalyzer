﻿using DocumentAnalyzer.Domain.Common;

namespace DocumentAnalyzer.Domain.Entities
{
    public class Document : BaseEntity
    {
        public string BucketName { get; set; }
        public string MimeType { get; set; }
        public string ContentType { get; set; }
        public string OriginalFileName { get; set; }
        public string OriginalFileKey { get; set; }
        public string? WordFrequencyFileKey { get; set; }
        public bool IsProcessed { get; set; }
    }
}
