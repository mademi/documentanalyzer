﻿using Microsoft.AspNetCore.Http;

namespace DocumentAnalyzer.Domain.Storage
{
    public interface IStorageClient
    {
        Task UploadFileAsync(IFormFile formFile, string objectName);
        Task<Stream> DownloadFileAsync(string objectName);
        Task DeleteFileAsync(string objectName);
    }
}
