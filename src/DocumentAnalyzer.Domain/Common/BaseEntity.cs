﻿namespace DocumentAnalyzer.Domain.Common
{
    public abstract class BaseEntity
    {
        public virtual Guid Id { get; protected set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
