﻿namespace DocumentAnalyzer.Contracts.Documents.Events
{
    public record DocumentProcessedEvent
    {
        public Guid Id { get; set; }
        public string WordFrequencyFileKey { get; set; }
        public bool IsProcessed { get; set; }
    }
}
