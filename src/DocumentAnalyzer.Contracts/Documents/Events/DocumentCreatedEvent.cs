﻿namespace DocumentAnalyzer.Contracts.Documents.Events
{
    public record DocumentCreatedEvent
    {
        public Guid Id { get; set; }
        public string BucketName { get; set; }
        public string OriginalFileKey { get; set; }
    }
}
