﻿namespace DocumentAnalyzer.Contracts.Health.Response
{
    public record HealthResponse(string Status);
}
